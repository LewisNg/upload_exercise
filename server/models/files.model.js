var Sequelize = require("sequelize");

module.exports = function (database) {
    return database.define('files', {
        name: Sequelize.STRING,
        original_name: Sequelize.STRING,
        path: Sequelize.STRING,
        deleted_at: Sequelize.STRING,      
    },
        {
            timestamps: true,
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        });
};