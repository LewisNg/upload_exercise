var multer = require('multer'),
  storage = multer.diskStorage({
    destination: './upload_tmp',
    filename: function (req, file, callback) {
      callback(null, Date.now() + '-' + file.originalname);
    }
  }),
  upload = multer({
    storage: storage
  });
var UploadController = require("./api/upload/upload.controller");

const API_FILES_URI = "/api/files";

module.exports = function (app, express) {
  app.post(API_FILES_URI, upload.single("file"), UploadController.uploadFile);
  app.get(API_FILES_URI, UploadController.retrieveFiles);
  app.put(API_FILES_URI + '/delete/:id', UploadController.deleteFile);

  app.use(express.static(__dirname + "/../client/"));
  app.use(express.static(__dirname + "/../upload_tmp/"));
};