var fs = require('fs');
var Files = require("../../database").Files;

exports.uploadFile = function (req, res) {
  console.log("Uploading to local..");
  fs.readFile(req.file.path, function (err, data) {
    if (err) {
      console.log("Err: ", err);
      res.status(500).json(err);
    } else {
      // console.log(req.file);
      Files
        .create({
          name: req.file.filename
          , original_name: req.file.originalname
          , path: req.file.path
        }).then(function (result) {
          res.status(200).json(result);
        }).catch(function (err) {
          res.status(500).json(err);
        });
    }
  });
};

exports.retrieveFiles = function (req, res) {
  Files
    .findAll({
      where: {
        deleted_at: null
      }
    })
    .then(function (result) {
      res.status(200).json(result);
    }).catch(function (err) {
      res.status(500).json(err);
    })
};

exports.deleteFile = function (req, res) {
  var where = {};
  where.id = req.params.id;
  Files
    .update({ deleted_at: Date.now() },
    { where: where }
    ).then(function (result) {
      res.status(200).json(result);
    }).catch(function (err) {      
      res.status(500).json(err);
    })
};

// exports.deleteFile = function (req, res) {
//   Files
//     .destroy({
//       where: {
//         id: req.params.id
//       }
//     }).then(function (deletedRecord) {
//       if (deletedRecord === 1) {
//         res.status(200).json({ message: "Deleted successfully" });
//       }
//     }).catch(function (err) {
//       res.status(500).json(err);
//     });
// };