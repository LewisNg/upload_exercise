var Sequelize = require('sequelize');
var config = require("./config");

var database = new Sequelize(config.mysql, {
    pool: {
        max: 2,
        min: 1,
        idle: 10000
    }
});

var Files = require("./models/files.model.js")(database);

// {force: true}
// database
//     .sync()
//     .then(function () {
//         console.log("Database in Sync Now");
//         //require("./seed")();
//     });

module.exports = {
    Files: Files
};

