(function () {
    angular
        .module("UploadApp")
        .config(UploadAppConfig);
    UploadAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function UploadAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("upload", {
                url: "/upload",
                views: {
                    "nav": {
                        templateUrl: "../app/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/upload/upload.html"
                    }
                },
                controller: 'UploadCtrl',
                controllerAs: 'ctrl'
            })
            .state("list", {
                url: "/list",
                views: {
                    "nav": {
                        templateUrl: "../app/navigation.html"
                    },
                    "content": {
                        templateUrl: "../app/list/list.html"
                    }
                },
                controller: 'ListCtrl',
                controllerAs: 'ctrl'
            })

        $urlRouterProvider.otherwise("/list");
    }
})();
