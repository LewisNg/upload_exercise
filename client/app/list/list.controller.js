(function () {

  angular
    .module('UploadApp')
    .controller('ListCtrl', ListCtrl);
  ListCtrl.$inject = ['moment', 'UploadService'];

  function ListCtrl(moment, UploadService) {
    var vm = this;
    vm.deleteFile = deleteFile;

    initFiles();

    function initFiles() {
      UploadService
        .retrieveFiles()
        .then(function (result) {
          vm.fileList = result.data;
          vm.fileList.forEach(function (obj) {
            obj.display_date = moment(obj.created_at).format("DD/MM/YY, HH:mm");;
          });
        }).catch(function (err) {
          console.log(JSON.stringify(err));
        })
    }

    function deleteFile(index) {
      if (confirm("Are you sure you want to delete this file?")) {
        UploadService
          .deleteFile(vm.fileList[index].id)
          .then(function (result) {            
            initFiles();
          })
          .catch(function (err) {
            console.log(JSON.stringify(err));
          });
      }
    }

  }
})();