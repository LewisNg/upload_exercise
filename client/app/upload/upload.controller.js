(function () {

  angular
    .module('UploadApp')
    .controller('UploadCtrl', UploadCtrl);
  UploadCtrl.$inject = ['$state', '$scope', 'Upload', 'UploadService'];

  function UploadCtrl($state, $scope, Upload, UploadService) {
    var vm = this;
    vm.file = null;
    vm.status = {
      code: 0
    };
    vm.dataHeader = [];
    vm.tableData = [];
    vm.initPreview = initPreview;
    vm.upload = upload;

    $scope.$watch('ctrl.file', function () {
      initPreview();
    });


    function initPreview() {
      if (vm.file !== null && vm.file !== undefined) {
        Papa.parse(vm.file, {
          complete: function (result) {
            vm.dataHeader = result.data[0];
            vm.tableData = result.data;            
          }
        });
      } else {
        vm.dataHeader = [];
        vm.tableData = [];
      }
    }

    function upload() {
      if (vm.file) {
        UploadService
          .uploadFile(vm.file)
          .then(function (response) {
            $state.go("list");
          }).catch(function (err) {
            console.log(JSON.stringify(err));
            vm.status.code = 500;
          })
      }
    }
  }
})();