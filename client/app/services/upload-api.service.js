(function () {
  angular
    .module("UploadApp")
    .service("UploadService", UploadService);

  UploadService.$inject = ['$http', 'Upload'];

  function UploadService($http, Upload) {
    var service = this;

    service.uploadFile = uploadFile;
    service.retrieveFiles = retrieveFiles;
    service.deleteFile = deleteFile;

    function uploadFile(file) {
      return Upload.upload({
        url: '/api/files',
        data: {
          "file": file
        }
      })
    }

    function retrieveFiles() {
      return $http({
        method: 'GET',
        url: 'api/files'
      });
    }

    function deleteFile(id) {
      return $http({
        method: 'PUT',
        url: "/api/files/delete/" + id
      });
    }

  }
})();